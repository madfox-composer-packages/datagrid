<?php declare(strict_types=1);

namespace Mdfx\Datagrid\Localization;

class SimpleTranslator implements \Nette\Localization\ITranslator
{

	/**
	 * @var array
	 */
	private $dictionary = [
		'ublaboo_datagrid.no_item_found_reset' => 'Nebyly nalezeny žádné položky. Resetujte filtr.',
		'ublaboo_datagrid.no_item_found' => 'Nebyly nalezeny žádné položky.',
		'ublaboo_datagrid.here' => 'zde',
		'ublaboo_datagrid.items' => 'Položek',
		'ublaboo_datagrid.all' => 'vše',
		'ublaboo_datagrid.from' => 'z',
		'ublaboo_datagrid.reset_filter' => 'Resetovat filtr',
		'ublaboo_datagrid.group_actions' => 'Hromadné akce',
		'ublaboo_datagrid.show' => 'Zobrazit',
		'ublaboo_datagrid.add' => 'Přidat',
		'ublaboo_datagrid.edit' => 'Upravit',
		'ublaboo_datagrid.show_all_columns' => 'Zobrazit všechny sloupce',
		'ublaboo_datagrid.show_default_columns' => 'Zobrazit původní sloupce',
		'ublaboo_datagrid.hide_column' => 'Skrýt sloupec',
		'ublaboo_datagrid.action' => 'Akce',
		'ublaboo_datagrid.previous' => 'Předchozí',
		'ublaboo_datagrid.next' => 'Další',
		'ublaboo_datagrid.choose' => 'Vybrat',
		'ublaboo_datagrid.choose_input_required' => 'Text hromadných akcí neakcpetuje prázdnou hodnotu.',
		'ublaboo_datagrid.execute' => 'Provést',
		'ublaboo_datagrid.save' => 'Uložit',
		'ublaboo_datagrid.cancel' => 'Zrušit',
		'ublaboo_datagrid.multiselect_choose' => 'Vybrat',
		'ublaboo_datagrid.multiselect_selected' => '{0} vybraných',
		'ublaboo_datagrid.filter_submit_button' => 'Filtrovat',
		'ublaboo_datagrid.show_filter' => 'Zobrazit filtr',
		'ublaboo_datagrid.per_page_submit' => 'Změnit',
		'ublaboo_datagrid.itemsPerPage' => 'Položek na stránce'
	];


	public function __construct(array $dictionary = [])
	{
		$this->dictionary = array_merge($this->dictionary, $dictionary);
	}


	function translate($message, ...$parameters): string
	{
		return $this->dictionary[$message] ?? $message;
	}


	public function setDictionary(array $dictionary): void
	{
		$this->dictionary = $dictionary;
	}
}

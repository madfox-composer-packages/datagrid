<?php declare(strict_types = 1);

namespace Mdfx\Datagrid;


class GridFactory
{

	public function create(): DataGrid
	{
		$grid = new DataGrid();

		$grid->setPrimaryKey('id');

		$grid->setAutoSubmit(FALSE);
		$grid->setCustomPaginatorTemplate(__DIR__ . '/templates/datagrid_paginator.latte');

		$grid->setTemplateFile(__DIR__ . '/templates/datagrid.latte');

		$translator = new \Mdfx\Datagrid\Localization\SimpleTranslator();

		$grid->setTranslator($translator);

		return $grid;
	}

}

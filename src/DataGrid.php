<?php declare(strict_types = 1);

namespace Mdfx\Datagrid;


class DataGrid extends \Ublaboo\DataGrid\DataGrid
{

	public function addEditAction(string $key, string $name, string $href, ?array $params = NULL): \Ublaboo\DataGrid\Column\Action
	{
		return $this->addAction($key, $name, $href, $params)
			->setClass('o-button o-button--sm o-button--success');
	}


	public function addDeleteAction(string $key, string $name, string $href, ?array $params = NULL): \Ublaboo\DataGrid\Column\Action
	{
		return $this->addAction($key, $name, $href, $params)
			->setClass('o-button o-button--sm o-button--danger');
	}

	public function addInfoAction(string $key, string $name, string $href, ?array $params = NULL): \Ublaboo\DataGrid\Column\Action
	{
		return $this->addAction($key, $name, $href, $params)
			->setClass('o-button o-button--sm o-button--info');
	}


	public function getSort(string $key, string $sort): array
	{
		return [ $key => $sort ];
	}


	public function addToolbarAddButton(string $href, string $label, array $params = []): \Ublaboo\DataGrid\Toolbar\ToolbarButton
	{
		return $this->addToolbarButton($href, $label, $params)
			->setClass('o-button o-button--md o-button--info');
	}

	public function saveSessionData(string $key, $value): void
	{
		if ($this->rememberState) {
			$this->gridSession[$key] = $value;
		}
	}

	public function findSessionValues(): void
	{
		try {
			parent::findSessionValues();
		} catch (\Ublaboo\DataGrid\Exception\DataGridFilterNotFoundException $e) {

		}
	}

}

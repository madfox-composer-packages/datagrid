<?php declare(strict_types = 1);

namespace Mdfx\Datagrid\DI;


class GridExtension extends \Nette\DI\CompilerExtension
{

	public function loadConfiguration()
	{
		$builder = $this->getContainerBuilder();

		$builder->addDefinition($this->prefix('helper.filterData'))
			->setFactory(\Mdfx\Datagrid\GridFactory::class)
		;

	}
}
